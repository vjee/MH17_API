<?php

class MeController extends Controller {

  /**
   * method for handling GET request
   *
   * @return void
   */
  protected function GET(): void
  {
    $user = $this->authenticate();

    $response = ['user' => $user];
    RES::send($response, 200);
  }

}
