<?php

class PostController extends Controller {

  protected $postDAO;

  /**
   * PostController constructor
   *
   * @param array       $route
   * @param string|null $query
   * @param \Model      $model
   */
  function __construct(array $route, string $query = null, Model $model)
  {
    $defaultFields = "`_id`, `title`, `description`, `date`, `image`";
    $defaultSort = "`_created` ASC";

    $this->postDAO = new PostDAO($defaultFields, $defaultSort);

    parent::__construct($route, $query, $model, $defaultFields, $defaultSort);
  }

  /**
   * method for handling GET method
   *
   * @return void
   */
  protected function GET(): void
  {
    $fields = $this->query['fields'];
    $sort = $this->query['sort'];

    $posts = $this->postDAO->getAll($fields, $sort);
    $response = ['posts' => $posts];
    RES::send($response, 200);
  }

  /**
   * method for handling GET method with id
   *
   * @return void
   */
  protected function GET_SINGLE(): void
  {
    if (!$this->postDAO->exists($this->routeId)) RES::sendCode(404);

    $fields = $this->query['fields'];
    $sort = $this->query['sort'];

    $post = $this->postDAO->getById($this->routeId, $fields, $sort);
    $response = ['post' => $post];
    RES::send($response, 200);
  }

  /**
   * method for handling POST request
   *
   * @return void
   */
  protected function POST(): void
  {
    $this->authenticate();
    $this->fallbackOptionalPayloadFields();
    $this->validatePayload();

    $post = $this->postDAO->insert($this->payload);
    $response = ['post' => $post];
    RES::send($response, 201);
  }

  /**
   * method for handling PATCH request
   *
   * @return void
   */
  protected function PATCH(): void
  {
    $this->__UPDATE();
  }

  /**
   * method for handling PUT request
   *
   * @return void
   */
  protected function PUT(): void
  {
    $this->__UPDATE();
  }

  /**
   * method for handling DELETE request
   *
   * @return void
   */
  protected function DELETE(): void
  {
    $this->authenticate();
    if (!$this->slideDAO->exists($this->routeId)) RES::sendCode(404);
    if ($this->postDAO->remove($this->routeId)) RES::sendCode(204);
    RES::sendCode(500);
  }

  /**
   * mechanism for updating a post
   * for use in eg PATCH and PUT requests
   *
   * @return void
   */
  protected function __UPDATE(): void
  {
    $this->authenticate();
    if (!$this->slideDAO->exists($this->routeId)) RES::sendCode(404);
    $this->validatePayload();

    $post = $this->postDAO->update($this->routeId, $this->payload);

    if ($post) {
      $response = ['post' => $post];
      RES::send($response, 200);
    } else {
      RES::sendCode(500);
    }
  }

}
