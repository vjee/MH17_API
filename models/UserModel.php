<?php

class UserModel extends Model {

  public $username;
  public $password;

  /**
   * UserModel constructor
   */
  function __construct()
  {
    $this->username = [
      'type' => 'string',
      'required' => true
    ];
    $this->password = [
      'type' => 'string',
      'required' => true
    ];
  }

}
