# MH17 API

## general

**required php version**

php 7.1

**migrating mysql database**

update properties in `./dao/DAO.php` to reflect credentials of new db

## development

**to run locally**

`$ php -S localhost:5000`

## configure server

**important**

- make sure CORS is set to host only, not to * for development
- remove error reporting rules in index.php

**upload directory**

upload this directory to the www root and rename to `api`

**get Authorization header through cgi**

in .htaccess:

`RewriteRule .? - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]`

in php:

`$_SERVER['HTTP_AUTHORIZATION']` might be `$_SERVER['REDIRECT_HTTP_AUTHORIZATION']` after using this rewrite rule

**forward everything after /api to the api**

in .htaccess:

`RewriteRule ^api.*$ api/index.php [QSA,NC,L]`
